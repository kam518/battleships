using BattleshipsConsole.Interfaces;
using BattleshipsConsole.Models;
using BattleshipsConsole.Services;
using FakeItEasy;
using NUnit.Framework;

namespace BattleshipsConsole.Tests
{
	public class GameServiceTest
	{
		private GameService GameService { get; set; }

		[SetUp]
		public void Setup()
		{
			var visualizer = A.Fake<IBoardVisualizer>();
			GameService = new GameService(visualizer, new BoardService(new ShipService()));
		}

		[Test, TestCaseSource(nameof(_isGameOverCases))]
		public void IsGameOverTest(Board board, bool result)
		{
			Assert.AreEqual(GameService.IsGameOver(board), result);
		}

		[Test, TestCaseSource(nameof(_isGameOverCases))]
		public void FireShotTest()
		{

		}

		[Test, TestCaseSource(nameof(_isGameOverCases))]
		public void GetAffectedShipTest()
		{

		}

		private static Board BoardFactory(int shipsToSink)
		{
			var board = new Board();

			for (var i = 0; i < shipsToSink; i++)
			{
				board.Ships[i].Coordinates = new Coordinates(0, i);
				board.Ships[i].SinkShip(board);
			}

			return board;
		}

		private static object[] _isGameOverCases =
		{
			new object[] {BoardFactory(3), true},
			new object[] {BoardFactory(2), false},
			new object[] {BoardFactory(1), false},
			new object[] {BoardFactory(0), false},
		};



	}
}