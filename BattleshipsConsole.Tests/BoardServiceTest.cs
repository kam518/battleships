using System.Collections.Generic;
using BattleshipsConsole.Enums;
using BattleshipsConsole.Interfaces;
using BattleshipsConsole.Models;
using BattleshipsConsole.Services;
using FakeItEasy;
using NUnit.Framework;

namespace BattleshipsConsole.Tests
{
	public class BoardServiceTest
	{
		private BoardService BoardService { get; set; }

		[SetUp]
		public void Setup()
		{
			BoardService = new BoardService(new ShipService());
		}

		[Test, TestCaseSource(nameof(_placeShipsCases))]
		public void PlaceShipsTest(IRandom rand, List<Coordinates> coordinates)
		{
			var board = new Board();
			BoardService.PlaceShips(rand, board);

			foreach (var coordinate in coordinates)
			{
				Assert.AreEqual(board.Fields[coordinate.Y][coordinate.X].FieldType, FieldType.Battleship,
					$"On coordinates {coordinate.X} {coordinate.Y} should be battleship ");
			}
		}

		private static object[] _placeShipsCases =
		{
			new object[]
			{
				FakeFactory(
					new[]
					{
						ShipOrientation.Horizontal,
						ShipOrientation.Horizontal,
						ShipOrientation.Horizontal
					},
					new[]
					{
						new Coordinates(0, 0),
						new Coordinates(5, 4),
						new Coordinates(0, 0),
						new Coordinates(6, 3)
					}),
				new List<Coordinates>
				{
					new Coordinates(0, 0),
					new Coordinates(1, 0),
					new Coordinates(2, 0),
					new Coordinates(3, 0),
					new Coordinates(4, 0),

					new Coordinates(5, 4),
					new Coordinates(6, 4),
					new Coordinates(7, 4),
					new Coordinates(8, 4),

					new Coordinates(6, 3),
					new Coordinates(7, 3),
					new Coordinates(8, 3),
					new Coordinates(9, 3)
				}
			},

			new object[]
			{
				FakeFactory(
					new[]
					{
						ShipOrientation.Horizontal,
						ShipOrientation.Vertical,
						ShipOrientation.Horizontal,
						ShipOrientation.Vertical
					},
					new[]
					{
						new Coordinates(2,3),
						new Coordinates(5,4),
						new Coordinates(5,5),
						new Coordinates(6,4)
					}),
				new List<Coordinates>
				{
					new Coordinates(2, 3),
					new Coordinates(3, 3),
					new Coordinates(4, 3),
					new Coordinates(5, 3),
					new Coordinates(6, 3),

					new Coordinates(5, 4),
					new Coordinates(5, 5),
					new Coordinates(5, 6),
					new Coordinates(5, 7),

					new Coordinates(6, 4),
					new Coordinates(6, 5),
					new Coordinates(6, 6),
					new Coordinates(6, 7)
				},
			},
			new object[]
			{
				FakeFactory(
					new[]
					{
						ShipOrientation.Horizontal,
						ShipOrientation.Vertical,
						ShipOrientation.Vertical
					},
					new[]
					{
						new Coordinates(0,3),
						new Coordinates(1,3),
						new Coordinates(2,4)
					}),
				new List<Coordinates>
				{
					new Coordinates(0, 3),
					new Coordinates(0, 4),
					new Coordinates(0, 5),
					new Coordinates(0, 6),
					new Coordinates(0, 7),

					new Coordinates(1, 3),
					new Coordinates(1, 4),
					new Coordinates(1, 5),
					new Coordinates(1, 6),

					new Coordinates(2, 4),
					new Coordinates(2, 5),
					new Coordinates(2, 6),
					new Coordinates(2, 7)
				},
			},
			new object[]
			{
				FakeFactory(
					new[]
					{
						ShipOrientation.Horizontal,
						ShipOrientation.Vertical,
						ShipOrientation.Vertical
					},
					new[]
					{
						new Coordinates(3, 2),
						new Coordinates(2,1),
						new Coordinates(8,0)
					}),
				new List<Coordinates>
				{
					new Coordinates(3, 2),
					new Coordinates(4, 2),
					new Coordinates(5, 2),
					new Coordinates(6, 2),
					new Coordinates(7, 2),

					new Coordinates(2, 1),
					new Coordinates(2, 2),
					new Coordinates(2, 3),
					new Coordinates(2, 4),

					new Coordinates(8, 0),
					new Coordinates(8, 1),
					new Coordinates(8, 2),
					new Coordinates(8, 3)
				},
			}
		};

		private static IRandom FakeFactory(ShipOrientation[] orientationArray, Coordinates[] returnValues)
		{
			var fake = A.Fake<IRandom>();
			A.CallTo(() => fake.DrawOrientation()).ReturnsNextFromSequence(orientationArray);
			A.CallTo(() => fake.DrawCoordinates(A<ShipOrientation>.Ignored, A<int>.Ignored)).ReturnsNextFromSequence(returnValues);

			return fake;
		}
	}
}