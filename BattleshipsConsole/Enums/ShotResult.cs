namespace BattleshipsConsole.Enums
{
	public enum ShotResult
	{
		Hit,
		Miss,
		Sink,
		AlreadySunk
	}
}
