namespace BattleshipsConsole.Enums
{
	public enum FieldStatus
	{
		Unknown = 'O',
		Hit = 'X',
		Sunk = 'S',
		Water = '*'
	}
}