namespace BattleshipsConsole.Enums
{
	public enum ShipOrientation
	{
		Vertical,
		Horizontal
	}
}