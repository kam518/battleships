namespace BattleshipsConsole.Models
{
	public class Coordinates
	{
		public int Y { get; set; }
		public int X { get; set; }

		public Coordinates(int x, int y)
		{
			Y = y;
			X = x;
		}
	}
}
