using BattleshipsConsole.Enums;

namespace BattleshipsConsole.Models
{
	public class Field
	{
		public Coordinates Coordinates { get; set; }
		public FieldType FieldType { get; set; }
		public FieldStatus FieldStatus { get; set; }
		public bool Marked { get; set; }

		public Field(int x, int y)
		{
			Coordinates = new Coordinates(x, y);
			FieldType = FieldType.Water;
			FieldStatus = FieldStatus.Unknown;
			Marked = false;
		}

		public bool IsOccupied => FieldType == FieldType.Battleship;

		public override bool Equals(object obj)
		{
			var field = (Field) obj;
			return Coordinates.Y == field?.Coordinates.Y && Coordinates.X == field.Coordinates.X;
		}

		public override int GetHashCode()
		{
			unchecked
			{
				int hashCode;
				if (Coordinates != null)
				{
					hashCode = Coordinates.Y.GetHashCode();
					hashCode = (hashCode * 397) ^ Coordinates.X.GetHashCode();
				}
				else hashCode = 0;
				return hashCode;
			}
		}
	}
}
