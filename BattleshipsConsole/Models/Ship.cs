using BattleshipsConsole.Enums;

namespace BattleshipsConsole.Models
{
	public class Ship
	{
		public Ship(ShipType shipType)
		{
			ShipType = shipType;
			Size = (int)shipType;
		}

		public void SinkShip(Board board)
		{
			if (Orientation == ShipOrientation.Horizontal) // <------>
			{
				for (var i = 0; i < Size; i++)
				{
					board.Fields[Coordinates.Y][Coordinates.X + i].FieldStatus = FieldStatus.Sunk;
				}
			}
			else // vertical
			{
				for (var i = 0; i < Size; i++)
				{
					board.Fields[Coordinates.Y + i][Coordinates.X].FieldStatus = FieldStatus.Sunk;
				}
			}

			IsSunk = true;
		}

		public ShipType ShipType { get; set; }
		public int Size { get; set; }
		public int Hits { get; set; }
		public ShipOrientation Orientation { get; set; }
		public Coordinates Coordinates { get; set; }
		public bool IsSunk { get; set; }
	}
}
