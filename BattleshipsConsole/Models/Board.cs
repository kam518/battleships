using System.Collections.Generic;

namespace BattleshipsConsole.Models
{
	public class Board
	{
		public List<List<Field>> Fields { get; set; }

		public List<Ship> Ships { get; }

		public Board()
		{
			Ships = new List<Ship>
			{
				new Ship(ShipType.Battleship),
				new Ship(ShipType.Destroyer),
				new Ship(ShipType.Destroyer)
			};

			Fields = new List<List<Field>>();

			for (var i = 0; i < PublicConsts.BoardSize; i++)
			{
				Fields.Add(new List<Field>());

				for (var j = 0; j < PublicConsts.BoardSize; j++)
				{
					Fields[i].Add(new Field(i, j));
				}
			}
		}
	}
}
