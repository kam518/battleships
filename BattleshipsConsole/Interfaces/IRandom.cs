using BattleshipsConsole.Enums;
using BattleshipsConsole.Models;

namespace BattleshipsConsole.Interfaces
{
	public interface IRandom
	{
		ShipOrientation DrawOrientation();
		Coordinates DrawCoordinates(ShipOrientation orientation, int shipSize);
	}
}