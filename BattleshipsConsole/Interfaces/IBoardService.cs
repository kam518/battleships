using System.Collections.Generic;
using BattleshipsConsole.Models;

namespace BattleshipsConsole.Interfaces
{
	public interface IBoardService
	{
		void PlaceShips(IRandom rand, Board board);
		Ship GetAffectedShip(List<Ship> ships, Coordinates coordinates);
	}
}