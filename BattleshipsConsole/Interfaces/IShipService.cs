using System.Collections.Generic;
using BattleshipsConsole.Enums;
using BattleshipsConsole.Models;

namespace BattleshipsConsole.Interfaces
{
	public interface IShipService
	{
		bool CheckIfShipIsPlacedOnCoordinates(Ship ship, Coordinates coordinates);
		bool CheckIfShipFits(List<List<Field>> fields, ShipOrientation orientation, Coordinates coordinates, int shipSize);
	}
}