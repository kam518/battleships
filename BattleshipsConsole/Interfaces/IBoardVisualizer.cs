using BattleshipsConsole.Models;

namespace BattleshipsConsole.Interfaces
{
	public interface IBoardVisualizer
	{
		void VisualizeBoard(Board board);
		void VisualizePlacementBoard(Board board);
	}
}