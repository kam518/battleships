using System;
using BattleshipsConsole.Models;
using BattleshipsConsole.Services;

namespace BattleshipsConsole
{
	class Program
	{
		static void Main()
		{
			var board = new Board();
			var gameService = new GameService(new ConsoleVisualizer(), new BoardService(new ShipService()));

			gameService.StartTheGame(board);
			while (!gameService.IsGameOver(board))
			{
				gameService.MakeMove(board);
			}

			Console.WriteLine("Congratulation, you have sunk all of the ships");
			Console.ReadKey();
		}
	}
}
