using System;
using BattleshipsConsole.Enums;
using BattleshipsConsole.Interfaces;
using BattleshipsConsole.Models;

namespace BattleshipsConsole.Services
{
	public class RandomnessService : Random,  IRandom
	{
		public RandomnessService(int seed) : base(seed){ }

		public ShipOrientation DrawOrientation()
		{
			return Next() % 2 == 0 ? ShipOrientation.Horizontal : ShipOrientation.Vertical;
		}

		public Coordinates DrawCoordinates(ShipOrientation orientation, int shipSize)
		{
			if (orientation == ShipOrientation.Horizontal) // horizontal <----->
			{
				return new Coordinates(Next(0, 10 - shipSize), Next(0, 10));
			}
			else // vertical
			{
				return new Coordinates(Next(0, 10), Next(0, 10 - shipSize));
			}
		}
	}
}