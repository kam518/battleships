using System;
using System.Linq;
using System.Text.RegularExpressions;
using BattleshipsConsole.Enums;
using BattleshipsConsole.Interfaces;
using BattleshipsConsole.Models;

namespace BattleshipsConsole.Services
{
	public class GameService
	{
		private readonly IBoardVisualizer _boardVisualizer;
		private readonly IBoardService _boardService;

		public GameService(IBoardVisualizer boardVisualizer, IBoardService boardService)
		{
			_boardVisualizer = boardVisualizer;
			_boardService = boardService;
		}

		public void StartTheGame(Board board)
		{
			_boardService.PlaceShips(new RandomnessService(Guid.NewGuid().GetHashCode()), board);
		}

		public void MakeMove(Board board)
		{
			_boardVisualizer.VisualizePlacementBoard(board); //debug only
			var coordinates = GetCoordinates();
			var result = FireShot(board, coordinates);
			Console.Clear();
			Console.WriteLine($"Shot result: {result}");
			_boardVisualizer.VisualizeBoard(board);
			Console.WriteLine();
		}

		public bool IsGameOver(Board board) => board.Ships.All(x => x.IsSunk);

		private ShotResult FireShot(Board board, Coordinates coordinates)
		{
			var ship = _boardService.GetAffectedShip(board.Ships, coordinates);

			if (ship == null)
			{
				board.Fields[coordinates.Y][coordinates.X].FieldStatus = FieldStatus.Water;
				return ShotResult.Miss;
			}

			if (!ship.IsSunk)
			{
				ship.Hits++;

				if (ship.Size == ship.Hits)
				{
					ship.SinkShip(board);
					return ShotResult.Sink;
				}

				board.Fields[coordinates.Y][coordinates.X].FieldStatus = FieldStatus.Hit;
				return ShotResult.Hit;
			}

			return ShotResult.AlreadySunk;
		}

		private Coordinates GetCoordinates()
		{
			var regex = new Regex(@"^[A-K]([1-9]|[1][0])$");

			while (true)
			{
				Console.WriteLine("Type coordinates to shoot (Format e.g. A5):");
				var coordinates = Console.ReadLine();

				
				var match = regex.Match(coordinates ?? string.Empty);
				if (match.Success)
				{
					return new Coordinates(int.Parse(coordinates.Substring(1)) - 1, coordinates[0] - PublicConsts.PrintingCoordinatesDelta);
				}
			}
		}
	}
}
