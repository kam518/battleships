using System;
using System.Text;
using BattleshipsConsole.Interfaces;
using BattleshipsConsole.Models;

namespace BattleshipsConsole.Services
{
	public class ConsoleVisualizer : IBoardVisualizer
	{
		private readonly StringBuilder _sb;
		public ConsoleVisualizer()
		{
			_sb = new StringBuilder();
		}
		public void VisualizeBoard(Board board)
		{
			PrintHeader();
			PrintBottomLine();
			for (var i = 0; i < PublicConsts.BoardSize; i++)
			{
				_sb.Clear();
				_sb.Append($"{(char)(i + PublicConsts.PrintingCoordinatesDelta)}  |");

				for (var j = 0; j < PublicConsts.BoardSize; j++)
				{
					_sb.Append($"  {(char)board.Fields[i][j].FieldStatus}  |");
				}
				Console.WriteLine(_sb);
				PrintBottomLine();
			}
		}

		public void VisualizePlacementBoard(Board board)
		{
			Console.ForegroundColor = ConsoleColor.Red;
			PrintHeader();
			PrintBottomLine();
			for (var i = 0; i < PublicConsts.BoardSize; i++)
			{
				_sb.Clear();
				_sb.Append($"{(char)(i + PublicConsts.PrintingCoordinatesDelta)}  |");

				for (var j = 0; j < PublicConsts.BoardSize; j++)
				{
					_sb.Append($"  {(char)board.Fields[i][j].FieldType}  |");
				}
				Console.WriteLine(_sb);
				PrintBottomLine();
			}
			Console.ForegroundColor = ConsoleColor.White;
		}

		private void PrintHeader()
		{
			Console.WriteLine("   |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  |  10 |");
		}

		private void PrintBottomLine()
		{
			Console.WriteLine("----------------------------------------------------------------");
		}
	}
}
