using System.Collections.Generic;
using BattleshipsConsole.Enums;
using BattleshipsConsole.Interfaces;
using BattleshipsConsole.Models;

namespace BattleshipsConsole.Services
{
	public class BoardService : IBoardService
	{
		private readonly IShipService _shipService;

		public BoardService(IShipService shipService)
		{
			_shipService = shipService;
		}

		public void PlaceShips(IRandom rand, Board board)
		{
			foreach (var ship in board.Ships)
			{
				var finished = false;
				while (!finished)
				{
					var orientation = rand.DrawOrientation();
					var coordinates = rand.DrawCoordinates(orientation, ship.Size);

					if (_shipService.CheckIfShipFits(board.Fields, orientation, coordinates, ship.Size))
					{
						if (orientation == ShipOrientation.Horizontal)
						{
							for (var i = 0; i < ship.Size; i++)//<------->
							{
								board.Fields[coordinates.Y][coordinates.X + i].FieldType = FieldType.Battleship;
							}
						}
						else // vertical
						{
							for (var i = 0; i < ship.Size; i++)//<------->
							{
								board.Fields[coordinates.Y + i][coordinates.X].FieldType = FieldType.Battleship;
							}
						}

						ship.Coordinates = coordinates;
						ship.Orientation = orientation;

						finished = true;
					}
				}
			}
		}

		public Ship GetAffectedShip(List<Ship> ships, Coordinates coordinates)
		{
			foreach (var ship in ships)
			{
				if (_shipService.CheckIfShipIsPlacedOnCoordinates(ship, coordinates)) return ship;
			}

			return null;
		}
	}
}
