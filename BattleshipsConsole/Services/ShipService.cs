using System.Collections.Generic;
using BattleshipsConsole.Enums;
using BattleshipsConsole.Interfaces;
using BattleshipsConsole.Models;

namespace BattleshipsConsole.Services
{
	public class ShipService : IShipService
	{
		public bool CheckIfShipIsPlacedOnCoordinates(Ship ship, Coordinates coordinates)
		{
			if (ship.Orientation == ShipOrientation.Horizontal) // <------>
			{
				if (ship.Coordinates.Y != coordinates.Y) return false;

				if (coordinates.X < ship.Coordinates.X || coordinates.X >= ship.Coordinates.X + ship.Size) return false;
			}
			else // vertical
			{
				if (ship.Coordinates.X != coordinates.X) return false;

				if (coordinates.Y < ship.Coordinates.Y || coordinates.Y >= ship.Coordinates.Y + ship.Size) return false;
			}

			return true;
		}

		public bool CheckIfShipFits(List<List<Field>> fields, ShipOrientation orientation, Coordinates coordinates, int shipSize)
		{
			for (var i = 0; i < shipSize; i++)
			{
				if (orientation == ShipOrientation.Horizontal) //<------->
				{
					if (fields[coordinates.Y][coordinates.X + i].IsOccupied) return false;
				}
				else // vertical
				{
					if (fields[coordinates.Y + i][coordinates.X].IsOccupied) return false;
				}
			}

			return true;
		}
	}
}
