namespace BattleshipsConsole
{
	public static class PublicConsts
	{
		public const int BoardSize = 10;
		public const int PrintingCoordinatesDelta = 65;
	}
}
